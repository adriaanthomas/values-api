using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ValuesApi.Controllers;

namespace ValuesApi.Tests
{
    [TestClass]
    public class ValuesControllerTest
    {
        [TestMethod]
        public void GetShouldReturn2Values()
        {
            // arrange
            var controller = new ValuesController();

            // act
            var result = controller.Get();

            // assert
            CollectionAssert.AreEqual(new string[] {"value1", "value2"}, result.ToList());
        }
    }
}
